CREATE TABLE IF NOT EXISTS `tipo_sensores` (
  `referencia` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `variable` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `precio` int(10) DEFAULT NULL,
  `salida` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`referencia`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_sensores`
--

INSERT INTO `tipo_sensores` (`referencia`, `nombre`, `variable`, `precio`, `salida`, `imagen`) VALUES
('PH0-14', 'Aquarium Hydroponic PH0-14 controller meter sensor', 'PH', 95, 'Numérico (0 – 14)', 'http://www.alselectro.com/images/Image-8_29mdj022.jpg'),
('DS18B20', 'Sensor Digital De Temperatura DS18B20 Impermeable', 'Temperatura', 13, 'Numérico (Centígrados Fahrenheit)', 'https://www.geekfactory.mx/wp-content/uploads/2013/06/ds18b20-sensor-de-temperatura-sumergible.jpg'),
('FC28', 'FC28 soil hygrometer humidity sensor', 'Humedad Tierra', 7, 'Numérico (% agua por volumen de tierra)', 'https://images-na.ssl-images-amazon.com/images/I/31QUuaYsGbL._AC_.jpg');
COMMIT;