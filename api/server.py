from flask import Flask, request
from flask_cors import CORS
from controllers.TipoSensores import TipoSensor

app = Flask(__name__)
CORS(app)

@app.route('/tipoSensores',methods=['GET'])
def getAll():
    return (TipoSensor.list())

@app.route('/tipoSensores',methods=['POST'])
def postOne():
    body = request.json
    return (TipoSensor.create(body))

app.run(host='0.0.0.0',port=5000)